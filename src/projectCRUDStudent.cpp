#include <iostream>
using namespace std;

char userChoice;

void getOption(){
    system("cls");
    cout<<"Project CRUD Student\n\n";
    cout<<"1. Add Student\n";
    cout<<"2. List of Student\n";
    cout<<"3. Change Student\n";
    cout<<"4. Delete Student\n";
    cout<<"5. Exit\n\n";
    cout<<"Choice [1-5] : ";
    cin>>userChoice;
}
void choice(){
    enum option {ADD = '1', LIST, CHANGE, DELETE, EXIT};

    while (userChoice != EXIT){
        switch(userChoice){
            case ADD:
            cout<<"\nAdd Student";
            break;

            case LIST:
            cout<<"\nList of Student";
            break;

            case CHANGE:
            cout<<"\nChange Student";
            break;

            case DELETE:
            cout<<"\nDelete Student";
            break;

            default:
            cout<<"Please choice [1-5]!";
        }
        labelIsContinue:
        cout<<"\nContinue? [y/n] : ";
        cin >> userChoice;
            if (userChoice == 'y' || userChoice == 'Y')
                getOption();
            else if (userChoice == 'n' || userChoice == 'N')
                break;
            else
                goto labelIsContinue;
    }
}

int main(){
    getOption();
    choice();
    
    cin.get();
    return 0;
}